import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  chairBookings: [],
  chairBookeds: [],
};

const datveSlice = createSlice({
  name: "datve",
  initialState,
  reducers: {
    setChairBookings: (state, action) => {
      const index = state.chairBookings.findIndex(
        (item) => item.soGhe === action.payload.soGhe
      );
      if (index === -1) {
        state.chairBookings.push(action.payload);
      } else {
        state.chairBookings.splice(index, 1);
      }
    },
    setChairBookeds: (state) => {
      state.chairBookeds = [...state.chairBookeds, ...state.chairBookings];
      state.chairBookings = [];
    },
  },
});

export const { setChairBookings, setChairBookeds } = datveSlice.actions;
export default datveSlice.reducer;
