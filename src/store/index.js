import { configureStore } from "@reduxjs/toolkit";
import datveReducer from "./datveSlice";

export const store = configureStore({
  reducer: {
    datve: datveReducer,
  },
});
