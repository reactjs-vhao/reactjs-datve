import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setChairBookeds, setChairBookings } from "../store/datveSlice";

const Result = () => {
  const { chairBookings } = useSelector((state) => state.datve);
  const dispatch = useDispatch();
  return (
    <div>
      <h3 className="text-white">Danh sách ghế bạn chọn</h3>
      <div className="d-flex flex-column">
        <button className="btn btn-outline-dark Chair booked">
          Ghế đã đặt
        </button>
        <button className="btn btn-outline-dark mt-3 Chair booking">
          Ghế đang chọn
        </button>
        <button className="btn bg-white my-3">Ghế chưa đặt</button>
      </div>
      <table className="table bg-white">
        <thead>
          <tr>
            <th>Số ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {chairBookings.map((ghe, index) => (
            <tr key={index}>
              <td>{ghe.soGhe}</td>
              <td>{ghe.gia.toLocaleString()}đ</td>
              <td>
                <button
                  className="btn btn-danger"
                  onClick={() => {
                    dispatch(setChairBookings(ghe));
                  }}
                >
                  Hủy
                </button>
              </td>
            </tr>
          ))}
          <tr className="fw-bold">
            <td>Tổng tiền</td>
            <td>
              {chairBookings
                .reduce((total, ghe) => (total += ghe.gia), 0)
                .toLocaleString()}
              {"đ"}
            </td>
            <td>
              <button
                className="btn btn-success"
                onClick={() => {
                  dispatch(setChairBookeds());
                }}
              >
                Thanh toán
              </button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default Result;
