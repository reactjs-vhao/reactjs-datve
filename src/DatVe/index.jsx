import React from "react";
import data from "./assets/data.json";
import ChairList from "./ChairList";
import Result from "./Result";

const DatVe = () => {
  return (
    <div className="container my-2">
      <div className="row">
        <div className="col-8 text-center">
          <h3 className="text-warning">ĐẶT VÉ XEM PHIM</h3>
          <div
            className="p-2 fw-bold text-white"
            style={{ backgroundColor: "#fe9f5f" }}
          >
            SCREEN
          </div>
          <ChairList data={data} />
        </div>
        <div className="col-4">
          <Result />
        </div>
      </div>
    </div>
  );
};

export default DatVe;
