import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setChairBookings } from "../store/datveSlice";
import cn from "classnames";
import "./style.scss";

const Chair = ({ ghe }) => {
  const dispatch = useDispatch();
  const { chairBookings, chairBookeds } = useSelector((state) => state.datve);
  return (
    <button
      className={cn("btn bg-white Chair", {
        booking: chairBookings.find((e) => e.soGhe === ghe.soGhe),
        booked: chairBookeds.find((e) => e.soGhe === ghe.soGhe),
      })}
      style={{ width: "50px" }}
      onClick={() => dispatch(setChairBookings(ghe))}
    >
      {ghe.soGhe}
    </button>
  );
};

export default Chair;
