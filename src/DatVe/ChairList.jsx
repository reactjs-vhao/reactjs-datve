import React from "react";
import Chair from "./Chair";

const ChairList = ({ data }) => {
  return (
    <div className="ChairList">
      {data.map((hangGhe, index) => {
        return (
          <div key={index} className="d-flex mt-2" style={{ gap: "10px" }}>
            <div
              className="text-center"
              style={{ width: "40px", color: "yellow" }}
            >
              {hangGhe.hang}
            </div>
            <div className="d-flex" style={{ gap: "10px" }}>
              {hangGhe.danhSachGhe.map((ghe, index) => {
                return <Chair key={index} ghe={ghe} />;
              })}
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default ChairList;
